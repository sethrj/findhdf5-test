#!/bin/sh -e
module purge
module load cmake/3.18
set +x
export CMAKE_SOURCE_DIR=/rnsdhpc/code/cmake

# Build with HDF5 without HL
rm -rf build || true
mkdir build
cd build
cmake .. 1> ../first.txt 2>&1
